local scrollEntries
local mainFrame
local scrollFrame
local scrollChild
local auraDB
local combatSegments
local currentSegment
local refreshEntries	


-- setting scripts for entries
local function setEntryScripts(entry, spellID)
	entry.auraIcon.onClick =  function (self, button, down)
		if IsLeftShiftKeyDown() and button=="LeftButton" then
			local kbFocus = GetCurrentKeyBoardFocus()
			if kbFocus then			
				kbFocus:SetText(kbFocus:GetText() .. GetSpellLink(spellID))
			end
		end
	end
	entry.auraIcon.onEnter = function (self)
		GameTooltip:SetOwner(self, "ANCHOR_RIGHT");
		GameTooltip:SetSpellByID(spellID)
	end		
end

-- called when adding Entries to the scrollframe
---- setting up entries
local function newEntry()
	local entry = CreateFrame("Frame", nil, scrollChild, "AuraScrollEntryTemplate")
  
  entry:SetPoint("TOP", scrollEntries[#scrollEntries], "BOTTOM",0,-5)
	table.insert(scrollEntries, entry)
  entry:Hide()
  return entry
end

--creating a new timestamp
local function newTimestamp(self, timestamp, cause, additionalInfo)
	table.insert(self.timeStamps, {stamp = timestamp, cause = cause, additionalInfo = additionalInfo})
end

--setting the data for an entry frame
local function setEntryData(entry, spellID, data)
  setEntryScripts(entry, spellID)
  entry.text:SetText(GetSpellLink(spellID))
  entry.auraIcon.texture:SetTexture(select(3, GetSpellInfo(spellID)))
end

--handling entry frames
local function refreshEntries()
  local n = 1
  local entry
  for spellID, data in pairs(auraDB) do
    entry = scrollEntries[n] or newEntry()
    setEntryData(entry, spellID, data)
		scrollChild:SetHeight(entry:GetHeight()*n+5*(n - 1))
    entry:Show()
    n = n + 1
  end
  for i = n, #scrollEntries do
    scrollEntries[i]:Hide()
  end
end

--creating a new combatSegmet
local function newCombatSegment(timestamp)
	currentSegment = {startTime = timestamp, dataHMS = date("%H:%M:%S")}
	currentSegment.timeStamps = {}
	currentSegment.auraEvents = {}
	currentSegment.newTimestamp = newTimestamp
	currentSegment:newTimestamp(timestamp, "SEGMENT_START")
	auraDB = currentSegment.auraEvents
	table.insert(combatSegments, currentSegment)
	refreshEntries()
end

function auraCombatSegmentDropdown_OnClick()

end

local eventFrame = CreateFrame("Frame", "auranomiconEventFrame", UIParent)

--eventhandler after onInit
local function onEvent(self, event, ...)
	if(event == "COMBAT_LOG_EVENT_UNFILTERED") then
		local timestamp, type, hideCaster, sourceGUID, sourceName, sourceFlags, sourceRaidFlags, destGUID, destName, destFlags, destRaidFlags, spellID, spellName, spellSchool, extra1, extra2, extra3, extra4 = CombatLogGetCurrentEventInfo()
		if string.find(type, "AURA") then
			local tmp = {}
			tmp.event = type
			tmp.sourceGUID = sourceGUID
			tmp.sourceName = sourceName
			tmp.destGUID = destGUID
			tmp.destName = destName
			tmp.spellName = spellName
			tmp.spellSchool = spellSchool
			
			if string.find(type, "BROKEN_SPELL") then -- "BROKEN" and "BROKEN_SPELL" pass different args
				tmp.extraSpellID = extra1
				tmp.extraSpellName = extra2
				tmp.extraSchool = extra3
				tmp.auraType = extra4
			else
				tmp.auraType = extra1
				tmp.amount = extra2 -- in the case the event is "BROKEN" (not "BROKEN_SPELL"), this is nil
			end
			
      auraDB[spellID] = auraDB[spellID] or {}
      table.insert(auraDB[spellID], tmp)
      
      if mainFrame:IsShown() then
        refreshEntries()
      end			
		end
	elseif(event == "PLAYER_REGEN_ENABLED") then
		print("Segment ended. nocombat?")
		eventFrame:UnregisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	elseif(event == "PLAYER_REGEN_DISABLED") then
		print("Segment started. combat?")
		newCombatSegment(GetServerTime())
		eventFrame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
	end
end

-- set up the frame and variables before listening to events
local function onInit(self, event, name)
	if name=="Auranomicon" then
    Auranomicon_DB = Auranomicon_DB or {}
    -- Auranomicon_DB = {}
    Auranomicon_DB.combatSegments = Auranomicon_DB.combatSegments or {}
		
    combatSegments = Auranomicon_DB.combatSegments
		
		mainFrame = auranomiconMainFrame
		scrollFrame = mainFrame.scrollFrame
		scrollChild = scrollFrame.scrollChild
		scrollFrame.ScrollBar:ClearAllPoints()
		scrollFrame.ScrollBar:SetPoint("TOPLEFT", scrollFrame,"TOPRIGHT", 0, -15)
		scrollFrame.ScrollBar:SetPoint("BOTTOMRIGHT", mainFrame,"BOTTOMRIGHT", -3, 35)
    
    local entry = CreateFrame("Frame", nil, scrollChild, "AuraScrollEntryTemplate")
    entry:SetPoint("TOP", scrollChild, "TOP", 0)
    scrollEntries = {}
    table.insert(scrollEntries, entry)

		-- setting up events at end of initialization and starting up usual eventhandling
		eventFrame:UnregisterEvent("ADDON_LOADED")
		eventFrame:SetScript("OnEvent", onEvent)
		eventFrame:RegisterEvent("PLAYER_REGEN_DISABLED")
		eventFrame:RegisterEvent("PLAYER_REGEN_ENABLED")
	end
end

eventFrame:SetScript("OnEvent", onInit)
eventFrame:RegisterEvent("ADDON_LOADED")